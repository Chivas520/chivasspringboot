package com.chivas.util.build;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BuildSql {

    public static void createSql1(BigDecimal amount, String userId) {

        // 未匹配加 未匹配减少已匹配加的通知
        String sql1 = "INSERT INTO `ast_mq_send` ( `mq_type`, `content`, `status`, `queue_name`, `create_time`, `update_time`, " +
                "`yn`, `subject_type`, `subject_id`, `target`) VALUES ('10', " +
                "'{\\\"assetOutAmount\\\":0,\\\"businessId\\\":\\\"%s\\\",\\\"finishTime\\\":%s,\\\"id\\\":0,\\\"matchAmount\\\":%s," +
                "\\\"type\\\":\\\"ASSET_OUT\\\",\\\"typeValue\\\":5,\\\"unMatchAmount\\\":%s," +
                "\\\"userId\\\":\\\"%s\\\"}', '1', 'asset.match.notice.queue', " +
                "now(), now(), '0', '1', '%s', '1');";




        String businessId = "WPYP-TZ-"+System.currentTimeMillis();
        Long finishTime = System.currentTimeMillis();
        String exeSql = String.format(sql1,businessId,finishTime,amount,amount.negate(),userId,userId);

        System.out.println(exeSql);

    }


    public static void createSql2(BigDecimal amount,String userId) {

        // 未匹配加 未匹配减少已匹配加的通知
        String sql1 = "INSERT INTO `ast_mq_send` ( `mq_type`, `content`, `status`, `queue_name`, `create_time`, `update_time`, " +
                "`yn`, `subject_type`, `subject_id`, `target`) VALUES ('10', " +
                "'{\\\"assetOutAmount\\\":0,\\\"businessId\\\":\\\"%s\\\",\\\"finishTime\\\":%s,\\\"id\\\":0,\\\"matchAmount\\\":%s," +
                "\\\"type\\\":\\\"ASSET_OUT\\\",\\\"typeValue\\\":5,\\\"unMatchAmount\\\":%s," +
                "\\\"userId\\\":\\\"%s\\\"}', '1', 'asset.match.notice.queue', " +
                "now(), now(), '0', '1', '%s', '1');";




        String businessId = "YP2WP"+System.currentTimeMillis();
        Long finishTime = System.currentTimeMillis();
        String exeSql = String.format(sql1,businessId,finishTime,amount.negate(),amount,userId,userId);

        System.out.println(exeSql);

    }



    public static void createSql3(BigDecimal amount,String userId) {

        // 未匹配加 未匹配减少已匹配加的通知
        String sql3 = "insert into ast_mq_task(mq_content,  task_type,  subject_id,  subject_type, " +
                " user_origin,  amount,  status,  busi_id,  create_time,  update_time,  busi_date,  yn) " +
                "values('{\"amount\":%s,\"busiDate\":%s,\"busiId\":\"%s\",\"source\":\"jlc\",\"type\":12,\"userId\":\"%s\"}', " +
                "12, '%s', 1, 'jlc', %s, 1, '%s', '%s', now(), now(), 0);";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String busi_date = sdf.format(new Date());

        String businessId = "SD-SF-"+System.currentTimeMillis();
        Long finishTime = System.currentTimeMillis();
        String exeSql = String.format(sql3,amount,finishTime,businessId,userId,userId,amount,businessId,busi_date);

        System.out.println(exeSql);

    }


    public static void createSql4(BigDecimal amount,String userId,String userIndentity,String userName) {

        // 未匹配加 未匹配减少已匹配加的通知
        String sql4 = "insert into ast_mq_task(mq_content,  task_type,  subject_id,  subject_type,  user_origin,  amount, " +
                " status,  busi_id,  create_time,  update_time,  busi_date,  yn) values(" +
                "'{\"amount\":%s,\"busiId\":\"%s\",\"buyCount\":1,\"redeemCount\":1,\"source\":\"jlc\"," +
                "\"type\":2,\"userId\":\"%s\",\"userIndentity\":\"%s\"," +
                "\"userName\":\"%s\"}', 2, '%s', 1, 'jlc', %s, 1, '%s', " +
                "now(), now(), '%s', 0);";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String busi_date = sdf.format(new Date());

        String businessId = "SD-TX-"+System.currentTimeMillis();

        String exeSql = String.format(sql4,amount,businessId,userId,userIndentity,userName,userId,amount,businessId,busi_date);

        System.out.println(exeSql);

    }


    public static void createSql5(BigDecimal amount,String userId) {

        // 未匹配加 未匹配减少已匹配加的通知
        String sql5 = "insert into ast_mq_task(mq_content,  task_type,  subject_id,  subject_type, " +
                " user_origin,  amount,  status,  busi_id,  create_time,  update_time,  busi_date,  yn) " +
                "values('{\"amount\":%s,\"busiDate\":%s,\"busiId\":\"%s\",\"source\":\"jlc\",\"type\":7," +
                "\"userId\":\"%s\"}', " +
                "7, '%s', 1, 'jlc', %s, 1, '%s', '%s', now(), now(), 0);";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String busi_date = sdf.format(new Date());

        String businessId = "SD-SF-"+System.currentTimeMillis();
        Long finishTime = System.currentTimeMillis();
        String exeSql = String.format(sql5,amount,finishTime,businessId,userId,userId,amount,businessId,busi_date);

        System.out.println(exeSql);

    }


    public static void createSql6(BigDecimal amount,String userId, String accountType) {

        // 难承接解冻
        String sql1 = "INSERT INTO `ast_mq_send` ( `mq_type`, `content`, `status`, `queue_name`, `create_time`, `update_time`, " +
                "`yn`, `subject_type`, `subject_id`, `target`) VALUES ('24', " +
                "'{\\\"assetOutAmount\\\":0,\\\"businessId\\\":\\\"%s\\\",\\\"businessType\\\":%s,\\\"finishTime\\\":%s,\\\"id\\\":0,\\\"matchAmount\\\":0," +
                "\\\"type\\\":\\\"DIFF_ASSET_AMOUNT_RELEASE\\\",\\\"typeValue\\\":18,\\\"unConfirmAmount\\\":%s,\\\"unMatchAmount\\\":%s," +
                "\\\"userId\\\":\\\"%s\\\"}', '1', 'diff.asset.release.queue', " +
                "now(), now(), '0', '1', '%s', '1');";



        String businessId = "SD-SF-"+System.currentTimeMillis();
        Long finishTime = System.currentTimeMillis();
        String exeSql = String.format(sql1,businessId,accountType,finishTime,amount,amount,userId,userId);

        System.out.println(exeSql);

    }





    public static void main(String[] args) {
        String userId = "dabf839aac1344e58f9a421e770df587";
        BigDecimal amount = new BigDecimal("11000");
        createSql5(amount,userId);
    }




}
