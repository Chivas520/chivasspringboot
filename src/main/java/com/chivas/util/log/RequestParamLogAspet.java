package com.chivas.util.log;


import com.alibaba.fastjson.JSONObject;
import javassist.*;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.LocalVariableAttribute;
import javassist.bytecode.MethodInfo;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

@Component
@Aspect
@Slf4j
public class RequestParamLogAspet {



    @Pointcut("execution(public * com.chivas.remote.impl .*.*(..))")
    public void paramLogAspet(){

    }



    @Before("paramLogAspet()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        String classType = joinPoint.getTarget().getClass().getName();
        Class<?> clazz = Class.forName(classType);
        String clazzName = clazz.getName();
        String methodName = joinPoint.getSignature().getName();

        //获取参数名称和值
        Map<String,Object > nameAndArgs = this.getFieldsName(this.getClass(), clazzName, methodName,args);

        log.info("接口调用请求信息:[{}]:[{}]:[{}]",clazzName,methodName,JSONObject.toJSONString(nameAndArgs));

    }




    private Map<String,Object> getFieldsName(Class cls, String clazzName, String methodName,
                                             Object[] args) throws NotFoundException {

        Map<String, Object> map = new HashMap<String, Object>();
        ClassPool pool = ClassPool.getDefault();
        ClassClassPath classPath = new ClassClassPath(cls);
        pool.insertClassPath(classPath);

        CtClass cc = pool.get(clazzName);
        CtMethod cm = cc.getDeclaredMethod(methodName);
        MethodInfo methodInfo = cm.getMethodInfo();
        CodeAttribute codeAttribute = methodInfo.getCodeAttribute();

        LocalVariableAttribute attr = (LocalVariableAttribute) codeAttribute.getAttribute(LocalVariableAttribute.tag);

        if (attr == null) {
        }


        int pos = Modifier.isStatic(cm.getModifiers()) ? 0 : 1;

        for (int i = 0; i < cm.getParameterTypes().length; i++) {
            map.put(attr.variableName(i + pos), args[i]);//paramNames即参数名
        }
        return map;
    }


    @AfterReturning(returning = "ret", pointcut = "paramLogAspet()")
    public void doAfterReturning(Object ret) throws Throwable {
        if(!StringUtils.isEmpty(ret)){
            log.info("RESPONSE : " + ret);
        }
    }


}
