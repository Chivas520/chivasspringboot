package com.chivas.bean;


import lombok.Data;

@Data
public class User {

    String name;
    Integer age;
}
