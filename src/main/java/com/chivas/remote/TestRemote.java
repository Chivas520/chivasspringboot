package com.chivas.remote;

import com.chivas.bean.User;

public interface TestRemote {

    /**
    * @description:打印参数
    * @author: RunFa.Zhou
    * @create: 2019/1/8
    **/
    void printMethedParam(String name);


    /**
    * @description:打印参数
    * @author: RunFa.Zhou
    * @create: 2019/1/8
    **/
    void printMethedUserParam(User user);

    /**
    * @description:打印参数
    * @author: RunFa.Zhou
    * @create: 2019/1/8
    **/
    User printMethedSetUser(String name);




}
