package com.chivas.remote.impl;

import com.alibaba.fastjson.JSONObject;
import com.chivas.bean.User;
import com.chivas.remote.TestRemote;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class TestRemoteImpl implements TestRemote {

    @Override
    public void printMethedParam(String name) {
        log.info("printMethedParam runing,name={}",name);
    }

    @Override
    public void printMethedUserParam(User user) {
        log.info("printMethedUserParam user info:{}",JSONObject.toJSONString(user));
    }

    @Override
    public User printMethedSetUser(String name) {
        User user = new User();
        user.setName(name);
        log.info("printMethedUserParam user info:{}",JSONObject.toJSONString(user));
        return user;
    }
}
