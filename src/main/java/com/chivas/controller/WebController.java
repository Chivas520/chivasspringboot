package com.chivas.controller;


import com.chivas.bean.User;
import com.chivas.remote.TestRemote;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
public class WebController {

    @Resource
    TestRemote testRemote;


    @RequestMapping("/index")
    public String index(final HttpServletRequest request){
        //testRemote.printMethedParam("Chivas");
        //User user = new User();
        //user.setAge(10);
        //user.setName("周润发");
        //testRemote.printMethedUserParam(user);
        testRemote.printMethedSetUser("Zhourunfa");
        return "WebController！";
    }
}
